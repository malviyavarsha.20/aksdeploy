variable "resource_group_name" {
  type    = string
  default = "aksdeploy-rg"
}

variable "location" {
  type    = string
  default = "West Europe"
}

variable "cluster_name" {
  type    = string
  default = "askdeploycluster"
}

variable "dns_prefix" {
  type    = string
  default = "aksdeploydns"
}

variable "node_count" {
  type    = number
  default = 1
}
variable "subscription_id" {
  type    = string
  default = "ba8ea1ab-0f6d-4cc6-b23d-8913b23a01e4"
}
